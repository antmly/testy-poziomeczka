#!/bin/bash

if [ $# -ne 3 ]; then
    echo Sposób użycia komendy: bash students.sh ab123456@students.mimuw.edu.pl "(szybciorem/testuj)" nazwa_pliku.c
    exit 1
fi

ssh $1 "mkdir -p ~/testy_programow_WPI21/"
scp -r ./* $1:~/testy_programow_WPI21/
ssh $1 "cd ~/testy_programow_WPI21/ ; bash ${2:-testuj}.sh $3; rm -r ~/testy_programow_WPI21"